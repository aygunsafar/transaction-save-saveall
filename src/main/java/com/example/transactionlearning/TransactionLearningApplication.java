package com.example.transactionlearning;

import com.example.transactionlearning.test.Test;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@RequiredArgsConstructor
public class TransactionLearningApplication implements CommandLineRunner {

    private final ApplicationContext applicationContext;

    public static void main(String[] args) {
        SpringApplication.run(TransactionLearningApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Test test= applicationContext.getBean(Test.class);
        test.schedule();
    }
}
