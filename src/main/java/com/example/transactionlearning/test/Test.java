package com.example.transactionlearning.test;

import com.example.transactionlearning.model.OrderEntity;
import com.example.transactionlearning.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Component
public class Test {

    private final OrderRepository orderRepository;


   // @Scheduled(fixedRate = 10000)
    public void schedule() {
        var list = new ArrayList<OrderEntity>(); //10000 orders
        for (int i = 0; i < 5000; i++) {
            OrderEntity order = OrderEntity.builder()
                    .customerName("Azer")
                    .quantity(19)
                    .product("phone")
                    .build();
            list.add(order);
        }
        System.out.println("list size " + list.size());
        saveBatch(list);
    }


    @Transactional
    public void save(List<OrderEntity> list) {
        Instant start = Instant.now();
        for (OrderEntity order : list) {
            orderRepository.save(order);
        }
        Instant end = Instant.now();
        Duration executionTime = Duration.between(start, end);
        System.out.println("Method executed in: " + executionTime.toMillis() + " ms"); //25835
    }


    @Transactional
    public void saveBatch(List<OrderEntity> list) {
        Instant start = Instant.now();
        orderRepository.saveAll(list);
        Instant end = Instant.now();
        Duration executionTime = Duration.between(start, end);
        System.out.println("Method executed in: " + executionTime.toMillis() + " ms"); //4357
    }

    @Transactional
    public void schedule1() {
        OrderEntity order = OrderEntity.builder()
                .customerName("Azer")
                .quantity(19)
                .product("phone")
                .build();
        orderRepository.save(order);
    }

}
